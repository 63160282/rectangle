/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.rectangleproject;

/**
 *
 * @author User
 */
public class Rectangle {
    double W;
    double L;
    
    Rectangle(double W,double L){
        this.W = W;
        this.L = L;
        
    }
    public double RectangleArea(){ 
        return W*L;
    }
    public double getR(){
        return W*L;
    }
    public void setR(double W,double L){
        if(W==0||L==0){
            System.out.println("Error");
            return;
        }
        this.W = W;
        this.L = L;
        
    }
}
